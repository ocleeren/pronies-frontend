export default {
  server: {
    host: '0' // default: localhost
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'EK 2020 - Pronostiek',
    htmlAttrs: {
      lang: 'nl'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ]
  },

  // [optional] markdownit options
  // See https://github.com/markdown-it/markdown-it
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    runtime: true
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/CountryFlag.js' },
    { src: '~/plugins/FormatDate.js' },
    { src: '~/plugins/notifications-ssr', mode: 'server' },
    { src: '~/plugins/notifications-client', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/fontawesome',
    '@nuxtjs/moment'
  ],

  moment: {
    defaultLocale: 'nl',
    locales: ['nl']
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/strapi',
    '@nuxtjs/markdownit',
    '@nuxtjs/recaptcha'
  ],

  recaptcha: {
    //hideBadge: Boolean, // Hide badge element (v3 & v2 via size=invisible)
    language: 'NL',   // Recaptcha language (v2)
    siteKey: '6LfWofQaAAAAAMn5yJnVjcx81mvLNcj4i6vC2bzI',    // Site key for requests
    version: 2,     // Version
    //size: String        // Size: 'compact', 'normal', 'invisible' (v2)
  },

  publicRuntimeConfig: {
    recaptcha: {
      /* reCAPTCHA options */
      siteKey: '6LfWofQaAAAAAJQk4H_3dG6QUz1Gpq0HGfZInoD7' // for example
    }
  },

  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: false // Or `bvCSS: false`
  },

  fontawesome: {
    icons: {
      solid: ['faHome', 'faQuoteLeft', 'faSave', 'faEdit', 'faTrophy', 'faCog', 'faEye', 'faCheck', 'faTimes'],
      brands: ['faFacebookF', 'faLinkedinIn', 'faTwitter']
    }
  },

  strapi: {
    url: 'http://91.177.244.121:1337'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }]
      ]
    },
    transpile: [
      'vee-validate'
    ]
  }
}
