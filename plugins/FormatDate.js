import Vue from 'vue';

import moment from 'moment'

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).locale('nl').format('DD MMM YYYY HH:mm')
    }
});